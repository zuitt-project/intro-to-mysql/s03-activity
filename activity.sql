
USE blog_db;

--Add records

INSERT INTO users (email, password, datetime_created)
	VALUES ("johnsmith@gmail.com", "passwordA", '2021-01-01 01:00:00'),
		   ("juandelacruz@gmail.com", "passwordB",'2021-01-01 02:00:00'),
	       ("janesmith@gmail.com", "passwordC",'2021-01-01 03:00:00'),
	       ("jmariadelacruz@gmail.com", "passwordD",'2021-01-01 04:00:00'),
	       ("johndoe@gmail.com", "passwordE",'2021-01-01 05:00:00');

INSERT INTO posts (title, content, datetime_posted, user_id)
	VALUES ("First Code", "Hello World!",'2021-01-02 01:00:00', 1),
		   ("Second Code", "Hello Earth!",'2021-01-02 02:00:00', 1),
		   ("Third Code", "Welcome to Mars!",'2021-01-02 03:00:00', 2),
		   ("Fourth Code", "Bye bye solar system!",'2021-01-02 04:00:00', 4);

--All post of Author ID = 1

SELECT title, content,datetime_posted
FROM posts
WHERE user_id = 1;

--All user's email and datetime of creation

SELECT email, datetime_created
FROM users;

--Update post

UPDATE posts
SET content = "Hello to the people of the Earth!"
WHERE id = 2;

--Delete user

DELETE FROM users
WHERE id = "johndoe@gmail.com";





